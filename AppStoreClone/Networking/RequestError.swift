//
//  RequestError.swift
//  AppStoreClone
//
//  Created by Анастасия on 12.08.2021.
//

import Foundation

enum RequestError: Error {
    case noData
    case noNetwork(message: String)
    case decodingError(code: Int)
    case backendError
}

extension RequestError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .noData:
            return "No Data"
        case .decodingError(code: let code):
            return "Decoding Error \(code)"
        case .backendError:
            return "Backend Error"
        case .noNetwork(message: let message):
            return "Возникла ошибка с сетью \(message)"
        }
    }
}
