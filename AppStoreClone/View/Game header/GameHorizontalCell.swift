//
//  GameHorizontalCell.swift
//  AppStoreClone
//
//  Created by Анастасия on 21.07.2021.
//


import UIKit


class GameHorizontalCell: UICollectionViewCell {
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Archelo"
        label.font = .systemFont(ofSize: 20, weight: .bold)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let textLabel: UILabel = {
        let label = UILabel()
        label.text = "Новый герой"
        label.font = .systemFont(ofSize: 16, weight: .regular)
        label.textColor = .gray
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let gameImageView: UIImageView = {
        let  iv =  UIImageView()
        iv.image = UIImage(named: "arch")
        iv.layer.cornerRadius = 8
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    private func setupUI() {
        addSubview(titleLabel)
        addSubview(textLabel)
        addSubview(gameImageView)
        
        titleLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        
        textLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor).isActive = true
        textLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        textLabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        
        gameImageView.topAnchor.constraint(equalTo: textLabel.bottomAnchor, constant: 12).isActive = true
        gameImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        gameImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -12).isActive = true
        gameImageView.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
    
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
