//
//  GameListCell.swift
//  AppStoreClone
//
//  Created by Анастасия on 22.07.2021.
//

import UIKit


class GameListCell: UICollectionViewCell {
    
    let imageView = UIImageView(cornerRadius: 0)
    let nameLabel = UILabel( text: "Game", font: .systemFont(ofSize: 20))
    let companyLabel = UILabel( text: "Company", font: .systemFont(ofSize: 13))
    let getButton = UIButton(title: "Загрузить")
    
    // MARK: - first
    
    /*let gameImageView: UIImageView = {
        let  img =  UIImageView()
        img.image = UIImage(named: "hs")
        img.layer.cornerRadius = 8
        img.clipsToBounds = true
        img.contentMode = .scaleAspectFill
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let gameBtnView: UIButton = {
        let  btn =  UIButton()
        btn.setTitle("Загрузить", for: .normal)
        btn.setTitleColor(.blue, for: .normal)
        btn.backgroundColor = .systemGray5
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let gameTextTitle: UILabel = {
        let label = UILabel()
        label.text = "Homescapes"
        label.font = .systemFont(ofSize: 18, weight: .medium)
        label.textColor = .black
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let gameTextCategory: UILabel = {
        let label = UILabel()
        label.text = "История за каждой дверью"
        label.font = .systemFont(ofSize: 14, weight: .regular)
        label.textColor = .gray
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let gameTextInfo: UILabel = {
        let label = UILabel()
        label.text = "Встроенные покупки"
        label.font = .systemFont(ofSize: 8, weight: .regular)
        label.textColor = .gray
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let gameLine: UIView = {
        let line = UIView()
        line.backgroundColor  = .systemGray5
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
    }()
    
    // MARK: - second
    
    let gameImageViewSecond: UIImageView = {
        let  img =  UIImageView()
        img.image = UIImage(named: "mm")
        img.layer.cornerRadius = 8
        img.clipsToBounds = true
        img.contentMode = .scaleAspectFill
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let gameBtnViewSecond: UIButton = {
        let  btn =  UIButton()
        btn.setTitle("Загрузить", for: .normal)
        btn.setTitleColor(.blue, for: .normal)
        btn.backgroundColor = .systemGray5
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let gameTextTitleSecond: UILabel = {
        let label = UILabel()
        label.text = "Monor Matters"
        label.font = .systemFont(ofSize: 18, weight: .medium)
        label.textColor = .black
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let gameTextCategorySecond: UILabel = {
        let label = UILabel()
        label.text = "Симуляторы"
        label.font = .systemFont(ofSize: 14, weight: .regular)
        label.textColor = .gray
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let gameTextInfoSecond: UILabel = {
        let label = UILabel()
        label.text = "Встроенные покупки"
        label.font = .systemFont(ofSize: 8, weight: .regular)
        label.textColor = .gray
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let gameLineSecond: UIView = {
        let line = UIView()
        line.backgroundColor  = .systemGray5
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
    }()
    
    
    // MARK: - third
    
    let gameImageViewThird: UIImageView = {
        let  img =  UIImageView()
        img.image = UIImage(named: "bw")
        img.layer.cornerRadius = 8
        img.clipsToBounds = true
        img.contentMode = .scaleAspectFill
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let gameBtnViewThird: UIButton = {
        let  btn =  UIButton()
        btn.setTitle("Загрузить", for: .normal)
        btn.setTitleColor(.blue, for: .normal)
        btn.backgroundColor = .systemGray5
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let gameTextTitleThird: UILabel = {
        let label = UILabel()
        label.text = "Bubble Witch 3 Saga"
        label.font = .systemFont(ofSize: 18, weight: .medium)
        label.textColor = .black
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let gameTextCategoryThird: UILabel = {
        let label = UILabel()
        label.text = "Шариковая мегастрелялка"
        label.font = .systemFont(ofSize: 14, weight: .regular)
        label.textColor = .gray
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let gameTextInfoThird: UILabel = {
        let label = UILabel()
        label.text = "Встроенные покупки"
        label.font = .systemFont(ofSize: 8, weight: .regular)
        label.textColor = .gray
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()*/
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        setupUI()
    }
    
    private func setupUI() {
        
        backgroundColor  = .white
        
        imageView.widthAnchor.constraint(equalToConstant: 64).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 64).isActive = true
        
        getButton.backgroundColor = UIColor(white: 0.95, alpha : 1)
        getButton.widthAnchor.constraint(equalToConstant: 120).isActive = true
        getButton.heightAnchor.constraint(equalToConstant: 32).isActive = true
        getButton.layer.cornerRadius = 32 / 2
        getButton.titleLabel?.font = .boldSystemFont(ofSize: 16)
        
        nameLabel.numberOfLines = 2
        
        let stackView = UIStackView(arrangedSubviews: [imageView, VerticalStackView(arrangedSubviews: [nameLabel, companyLabel], spacing: 4), getButton])
        stackView.spacing = 16
        stackView.alignment = .center
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
        
        
        /*addSubview(gameImageView)
        addSubview(gameBtnView)
        addSubview(gameTextTitle)
        addSubview(gameTextCategory)
        addSubview(gameTextInfo)
        addSubview(gameLine)
        addSubview(gameImageViewSecond)
        addSubview(gameBtnViewSecond)
        addSubview(gameTextTitleSecond)
        addSubview(gameTextCategorySecond)
        addSubview(gameTextInfoSecond)
        addSubview(gameLineSecond)
        addSubview(gameImageViewThird)
        addSubview(gameBtnViewThird)
        addSubview(gameTextTitleThird)
        addSubview(gameTextCategoryThird)
        addSubview(gameTextInfoThird)
        
        
        // MARK: - first position
        
        gameImageView.topAnchor.constraint(equalTo: topAnchor, constant: 12).isActive = true
        gameImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        gameImageView.heightAnchor.constraint(equalToConstant: 83).isActive = true
        gameImageView.widthAnchor.constraint(equalToConstant: 83).isActive = true
        
        gameLine.topAnchor.constraint(equalTo: gameImageView.bottomAnchor, constant: 6).isActive = true
        gameLine.leftAnchor.constraint(equalTo: gameImageView.rightAnchor, constant: 6).isActive = true
        gameLine.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        gameLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        gameTextTitle.centerYAnchor.constraint(equalTo: gameImageView.centerYAnchor, constant: -9).isActive = true
        gameTextTitle.leftAnchor.constraint(equalTo: gameImageView.rightAnchor, constant: 12).isActive = true
        gameTextTitle.rightAnchor.constraint(equalTo: gameBtnView.leftAnchor, constant: -12).isActive = true
        gameTextTitle.heightAnchor.constraint(equalToConstant: 18).isActive = true
        
        gameTextCategory.topAnchor.constraint(equalTo: gameTextTitle.bottomAnchor, constant: 0).isActive = true
        gameTextCategory.leftAnchor.constraint(equalTo: gameImageView.rightAnchor, constant: 12).isActive = true
        gameTextCategory.rightAnchor.constraint(equalTo: gameBtnView.leftAnchor, constant: -12).isActive = true
        gameTextCategory.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        gameTextInfo.topAnchor.constraint(equalTo: gameBtnView.bottomAnchor, constant: 4).isActive = true
        gameTextInfo.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        gameTextInfo.heightAnchor.constraint(equalToConstant: 8).isActive = true
        gameTextInfo.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        gameBtnView.centerYAnchor.constraint(equalTo: gameImageView.centerYAnchor, constant: 0).isActive = true
        gameBtnView.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        gameBtnView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        gameBtnView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        // MARK: - second position
        
        gameImageViewSecond.topAnchor.constraint(equalTo: gameLine.bottomAnchor, constant: 6).isActive = true
        gameImageViewSecond.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        gameImageViewSecond.heightAnchor.constraint(equalToConstant: 83).isActive = true
        gameImageViewSecond.widthAnchor.constraint(equalToConstant: 83).isActive = true
        
        gameLineSecond.topAnchor.constraint(equalTo: gameImageViewSecond.bottomAnchor, constant: 6).isActive = true
        gameLineSecond.leftAnchor.constraint(equalTo: gameImageViewSecond.rightAnchor, constant: 6).isActive = true
        gameLineSecond.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        gameLineSecond.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        gameTextTitleSecond.centerYAnchor.constraint(equalTo: gameImageViewSecond.centerYAnchor, constant: -9).isActive = true
        gameTextTitleSecond.leftAnchor.constraint(equalTo: gameImageViewSecond.rightAnchor, constant: 12).isActive = true
        gameTextTitleSecond.rightAnchor.constraint(equalTo: gameBtnViewSecond.leftAnchor, constant: -12).isActive = true
        gameTextTitleSecond.heightAnchor.constraint(equalToConstant: 18).isActive = true
        
        gameTextCategorySecond.topAnchor.constraint(equalTo: gameTextTitleSecond.bottomAnchor, constant: 0).isActive = true
        gameTextCategorySecond.leftAnchor.constraint(equalTo: gameImageViewSecond.rightAnchor, constant: 12).isActive = true
        gameTextCategorySecond.rightAnchor.constraint(equalTo: gameBtnViewSecond.leftAnchor, constant: -12).isActive = true
        gameTextCategorySecond.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        gameTextInfoSecond.topAnchor.constraint(equalTo: gameBtnViewSecond.bottomAnchor, constant: 4).isActive = true
        gameTextInfoSecond.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        gameTextInfoSecond.heightAnchor.constraint(equalToConstant: 8).isActive = true
        gameTextInfoSecond.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        gameBtnViewSecond.centerYAnchor.constraint(equalTo: gameImageViewSecond.centerYAnchor, constant: 0).isActive = true
        gameBtnViewSecond.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        gameBtnViewSecond.heightAnchor.constraint(equalToConstant: 40).isActive = true
        gameBtnViewSecond.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        // MARK: - third position
        
        gameImageViewThird.topAnchor.constraint(equalTo: gameLineSecond.bottomAnchor, constant: 6).isActive = true
        gameImageViewThird.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        gameImageViewThird.heightAnchor.constraint(equalToConstant: 83).isActive = true
        gameImageViewThird.widthAnchor.constraint(equalToConstant: 83).isActive = true
        
        gameTextTitleThird.centerYAnchor.constraint(equalTo: gameImageViewThird.centerYAnchor, constant: -9).isActive = true
        gameTextTitleThird.leftAnchor.constraint(equalTo: gameImageViewThird.rightAnchor, constant: 12).isActive = true
        gameTextTitleThird.rightAnchor.constraint(equalTo: gameBtnViewThird.leftAnchor, constant: -12).isActive = true
        gameTextTitleThird.heightAnchor.constraint(equalToConstant: 18).isActive = true
        
        gameTextCategoryThird.topAnchor.constraint(equalTo: gameTextTitleThird.bottomAnchor, constant: 0).isActive = true
        gameTextCategoryThird.leftAnchor.constraint(equalTo: gameImageViewThird.rightAnchor, constant: 12).isActive = true
        gameTextCategoryThird.rightAnchor.constraint(equalTo: gameBtnViewThird.leftAnchor, constant: -12).isActive = true
        gameTextCategoryThird.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        gameTextInfoThird.topAnchor.constraint(equalTo: gameBtnViewThird.bottomAnchor, constant: 4).isActive = true
        gameTextInfoThird.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        gameTextInfoThird.heightAnchor.constraint(equalToConstant: 8).isActive = true
        gameTextInfoThird.widthAnchor.constraint(equalToConstant: 100).isActive = true
   
        gameBtnViewThird.centerYAnchor.constraint(equalTo: gameImageViewThird.centerYAnchor, constant: 0).isActive = true
        gameBtnViewThird.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        gameBtnViewThird.heightAnchor.constraint(equalToConstant: 40).isActive = true
        gameBtnViewThird.widthAnchor.constraint(equalToConstant: 100).isActive = true*/
        
       
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
