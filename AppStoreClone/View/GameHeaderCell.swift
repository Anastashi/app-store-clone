//
//  GameHeaderCell.swift
//  AppStoreClone
//
//  Created by Анастасия on 15.07.2021.
//


import UIKit

class GameHeaderCell : UICollectionReusableView{
    
    let gameHeader = GameHorizontalController()
    
    /*let gameImageView: UIImageView = {
        let  iv =  UIImageView()
        iv.image = UIImage(named: "1")
        iv.layer.cornerRadius = 12 // border-radius
        iv.clipsToBounds = true // overflew
        iv.contentMode = .scaleAspectFill // масштабируемость
        iv.translatesAutoresizingMaskIntoConstraints = false // программно задаем размеры и пр
        return iv
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Title"
        label.font = .systemFont(ofSize: 20, weight: .bold)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let textLabel: UILabel = {
        let label = UILabel()
        label.text = "Description Description Description DescriptionDescriptionDescriptionDescription Description Description Description "
        label.font = .systemFont(ofSize: 16, weight: .regular)
        label.textColor = .black
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()*/
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    private func setupUI() {
        addSubview(gameHeader.view)
        
        gameHeader.view.translatesAutoresizingMaskIntoConstraints = false
        
        gameHeader.view.topAnchor.constraint(equalTo: topAnchor).isActive = true
        gameHeader.view.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        gameHeader.view.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        gameHeader.view.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        /*backgroundColor = .red
        addSubview(gameImageView)
        addSubview(titleLabel)
        addSubview(textLabel)
        
        textLabel.backgroundColor = .green
        
        titleLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        
        textLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8).isActive = true
        textLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        //textLabel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        //textLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.5).isActive =  true
        textLabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        
        gameImageView.topAnchor.constraint(equalTo: textLabel.bottomAnchor, constant: 24).isActive = true
        gameImageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        gameImageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        gameImageView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true*/
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
