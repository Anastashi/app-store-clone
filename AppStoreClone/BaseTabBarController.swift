//
//  BaseTabBarController.swift
//  AppStoreClone
//
//  Created by Анастасия on 07.07.2021.
//

import UIKit

class BaseTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .green
        
        viewControllers = [
            createNewController(viewController: GamePageController(), title: "Игры", imageName: ""),
            createNewController(viewController: UIViewController(), title: "Test2", imageName: "")
        ]
    }
    
    private func createNewController(viewController: UIViewController, title: String, imageName: String) -> UIViewController {
        let navController = UINavigationController(rootViewController: viewController)
        
        navController.navigationBar.prefersLargeTitles = true
        viewController.navigationItem.title  = title
        
        navController.tabBarItem.title  = title
        navController.tabBarItem.image = UIImage(named: imageName)
        
        return navController
        
    }

   

}
