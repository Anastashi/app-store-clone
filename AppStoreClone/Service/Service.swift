//
//  Service.swift
//  AppStoreClone
//
//  Created by Анастасия on 29.07.2021.
//

import Foundation

class Service {
    
    static let shared = Service()
    
    func get(url: String, completionHandler: @escaping (Result<Games,RequestError>) -> Void ) {
        guard let url = URL(string: url) else { return }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            
            /*if error != nil {
                completionHandler(.failure(.noNetwork(message: error?.localizedDescription ?? "текстик")))
                return
            }*/
            
            if let error = error {
                completionHandler(.failure(.noNetwork(message: error.localizedDescription)))
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode) else {
                completionHandler(.failure(.backendError))
                return }
            
            guard let data = data else {
                completionHandler(.failure(.noData))
                return }
            
            //распарсим
            
            if let games = try? JSONDecoder().decode(Games.self, from: data) {
                completionHandler(.success(games))
            } else {
                completionHandler(.failure(.decodingError(code: httpResponse.statusCode)))
            }
            
        }
        task.resume()
    }
}

