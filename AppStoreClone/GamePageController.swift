//
//  GamePageController.swift
//  AppStoreClone
//
//  Created by Анастасия on 07.07.2021.
//

import UIKit

class GamePageController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let cellID = "gameID"
    let headerId = "gameHeaderID"
    
    var games: Games?
    
    override func viewDidLoad() {
        collectionView.backgroundColor = .white
        collectionView.register(GamePageCell.self, forCellWithReuseIdentifier: cellID)
        collectionView.register(GameHeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerId)
        
        fetchGames()
    }
    
    private func fetchGames(){
        let url = "https://rss.itunes.apple.com/api/v1/us/ios-apps/new-games-we-love/all/50/explicit.json"
        
        
        Service.shared.get(url: url) { [ weak self ] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let requstGames):
                self.updateGames(requstGames: requstGames)
            case .failure(let requstError):
                DispatchQueue.main.async {
                    self.presentErrorAlert(message: requstError.localizedDescription)
                }
                
                //print("We get some error", requstError.localizedDescription)
            }
            
        }
        // weak self -  не захломлять счетчик / делаем ссылку слабой
        // все что weak - опционально
        // self - это контроллер
        
    }
    
    private func updateGames(requstGames: Games) {
        games = requstGames
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    private func presentErrorAlert(message: String) {
        let alert = UIAlertController(title: "Ошибка", message: message, preferredStyle: .actionSheet)
        // let alert = UIAlertController(title: "Ошибка", message: message, preferredStyle: .alert)
       
        alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    
    // MARK: - Header
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath) as! GameHeaderCell
            return header
        }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return .init(width: view.frame.width, height: 300)
    }
    
    override func collectionView(_ colllectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt
        indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! GamePageCell
     
        
        cell.gameList.games = games
        
        cell.gameList.didselectHandler = { [weak self] game in
            let gameDetail = GameDetailViewController()
            gameDetail.navigationItem.title = game.artistName
            self?.navigationController?.pushViewController(gameDetail, animated: true)
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width, height: 300)
    }
    
    private func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> UIEdgeInsets {
        return .init(top: 16, left: 0, bottom: 0, right: 0)
    }
    
    
    init() {
        super.init(collectionViewLayout: UICollectionViewFlowLayout())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
